# create some variables
variable "tomcat_dns_base_domain" {
  type        = string
  description = "DNS Zone name to be used from EKS tomcat."
}
variable "tomcat_gateway_chart_name" {
  type        = string
  description = "tomcat Gateway Helm chart name."
}
variable "tomcat_gateway_chart_repo" {
  type        = string
  description = "tomcat Gateway Helm repository name."
}
variable "tomcat_gateway_chart_version" {
  type        = string
  description = "tomcat Gateway Helm chart version."
}

# deploy tomcat Controller
resource "helm_release" "tomcat_gateway" {
  name       = var.tomcat_gateway_chart_name
  chart      = var.tomcat_gateway_chart_name
  repository = var.tomcat_gateway_chart_repo
  values = [
    "${file("values.yaml")}"
  ]
}
