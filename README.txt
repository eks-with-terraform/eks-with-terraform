Used Gitlab based CI system to deploy the given components on AWS Cloud.
gitlab-ci contains set of stage:
  - pre-setup
  - validate
  - build
  - deploy
  - flush

This gitlab-ci.yaml is executed by gitlab by using gitlab-runner 
  - pre-setup: created the s3 bucket with versioning enabled, because we are saving .tfstate file. Right now I don’t have much time to enable terraboard. 
  - validate: validating the terraform script by using following command “terraform validate”
  - build: running the “terraform plan” command to see before deploying the item cloud.
  - deploy: running the “terraform apply” to deploy the service like vpc, eks, route53 to cloud 
 - flush: running the “terraform destroy” to delete the deployed deployment.

We have created individual terraform code for each service as flexible solution.
Deployed the tomcat application by using helm cart.
Deploy the .war file tomcat manager as of now or we can attach pv, pvc items. 
Enable the below value, if you are deploying from outside the cloud if not it will use IAM Role.
AWS_DEFAULT_REGION            
AWS_ACCESS_KEY_ID                    
AWS_SECRET_ACCESS_KEY



1. Create Network VPC with Private & Public Subnet 

network.tf -- Please use network.tf file to create vpc with multi subnets

$ terraform init
$ terraform plan 
$ terraform apply 

2. Launch Cluster EKS 

go to the eks directory --->  eks.tf 

this file is resposible for creating the cluster 

3. Create namespace to deploy the application and casandra db

$ cd namespaces 

you will find "namespaces-development.tfvars", update this file for the spceific namespace.

4. Create record with ingress

Once deployment is done on EKS we need to run terraform script to create the ingress for appliction, Here we are poining the appliction to the loadbalancer. 

5. Create hosted zone and record set. 
 
RT53  --  This directory is responsible for creating the record set. 
 
6. Helm Charts 

We are deploying the application using helm , so this chart folder is reponsible to deploy the casandra db and tomcat application with HPA mode eanbled. 

########### To Test The Code #####

1. We should have terraform version 14 installed on our system.
2. aws cli install and configured.
3. kubcetl tool should be installed so we can configure it to access our EKS cluster.

### Execute terraform code ###

1. Network
2. EKS
3. run below command from your computer
 
 aws eks update-kubeconfig \
  --region $region \
  --name my-cluster
  
4. Test you configuration.
   $ kubectl get svc 
   
5. Deployment of tomcat and db is done once we have run the EKS terraform script. 

6. Test the pod and services of tomcat and db. 

7. Deploy the exeternal DNS 

cd external-dns --> Apply the yaml files for creating the external dns service.

7. backend.tf
 S3 bucket as backend resources seleted for this deployment.
 
 
## Terraform Steps ## 

1. Create the resources 

Go inside the code directory  and execute the commands.

$ terraform init
$ terraform plan 
$ terraform apply 

2. Destroy the resources.

$ terraform destroy

kubectl describe pod cassandra-0
kubectl get pods
kubectl get statefulsets


