deployments_subdomains = ["sample", "api"] # to be prefixed before dns_base_domain (e.g. sample.eks.cloud.cl or api.eks.cloud.cl), and handled by Ingress rules defined by each Application Helm Chart
